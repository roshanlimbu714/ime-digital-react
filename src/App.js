import logo from './logo.svg';
import './App.css';
// import Welcome from './pages/Welcome';
import {Welcome} from './pages/Welcome';
import { Patrick } from './pages/Patrick';

function App() {
  return (
    <div className="App">
      {/* <Welcome name={"test"} >
        <div> Test </div>
      </Welcome> */}
      <Patrick/>
    </div>
  );
}

export default App;
