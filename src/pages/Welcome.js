// export const Welcome = ()=>(<section>patrick</section>)
// const Welcome = ()=>{

export const Welcome = (props)=>{
    return <section>
        <h1>{props.name}</h1>
        <h2>children</h2>
        {props.children}
    </section>
}

// export default Welcome;