import patrick from '../assets/patrick.webp';

export const PatrickRaw = () => {
    return (
        <main className="patrick-screen">
            <nav>
                <div className="logo">Logo</div>
                <div className="nav-items">
                    <div className="nav-item">Home</div>
                    <div className="nav-item">About</div>
                    <div className="nav-item">Services</div>
                    <div className="nav-item">Products</div>
                    <div className="nav-item">Contact</div>
                </div>
            </nav>
            <section>
                <div className="text-area">
                    <div className="tabs">
                        <div className="tab active">Games</div>
                        <div className="tab">TV</div>
                        <div className="tab">Characters</div>
                    </div>
                    <div className="title">
                        Patrick
                    </div>
                    <div className="title">
                        Starfish
                    </div>
                    <div className="sub-title">Spongebob's best friend</div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex consequatur consequuntur id odio ea. Fugit reprehenderit impedit, itaque corporis quia nostrum quam maxime, aspernatur dolore minima et. Tempora, nam accusamus?</p>
                    <div>
                        <button>Get Started</button>
                    </div>
                </div>
                <div className="image-area">
                    <img src={patrick} alt="" />
                </div>
            </section>
        </main>
    )
}