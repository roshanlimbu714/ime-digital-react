import { Navbar } from '../partials/Navbar';
import { PatrickLanding } from '../containers/PatrickLanding';
import { PatrickFeatures } from '../containers/PatrickFeatures';
export const Patrick = ()=>{
    return (
        <main className="patrick-screen">
            <Navbar/>
            <PatrickLanding/>
            <PatrickFeatures />
        </main>
    )
}