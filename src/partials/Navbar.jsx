export const Navbar = ()=>{
    const navItems = [
        {label:'Home'},
        {label:'About'},
        {label:'Services'},
        {label:'Products'},
        {label:'Contact'},
    ]
    return (
        <nav>
            <div className="logo">Logo</div>
            <div className="nav-items">
              {navItems.map((navItem, key)=>(
                  <div className="nav-item" key={key}>{navItem.label}</div>
              ))}
            </div>
    </nav>
    )
}